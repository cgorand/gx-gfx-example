# Example to use gx-gfx-standalone library

## Requirements

The library has been tested on Debian and has been used in embedded system too.
There is no external dependencies except the gx-gfx-standalone project.

## How to build

Clone libray project here : 

```
git@gitlab.com:cgorand/gx-gfx-standalone.git
```

Both project shall be at the same level:

```
gx-gfx/
gx-gfx-standalone/
```

Launch the script :

```
$ ./build.sh
```

## Examples and Tests

In test folder there are the following scripts :

* build_example.sh : build simple example (sources in main.cpp)
* test.sh : compress sample images using best compression algorithm and will uncompressed it into PNG to get result.
* test_all_aglo.sh : run all possible combination of paramters on sample images.
* clean.sh : clean up script

After launching build_example.sh, a binary test_unpack is created.

## Performances

Depending on image content, the compression good results compared to PNG format :

```
test565.png          : 768000
GRAY SCALE :
* Compress BEST      : 61149  (7%)
* Compress PNG       : 1877   (0%)
RGB 565 :
* Compress BEST      : 126137 (16%)
* Compress PNG       : 36955  (4%)
RGB :
* Compress BEST      : 21232  (2%)
* Compress PNG       : 2385   (0%)
RGBA:
* Compress BEST      : 28105  (3%)
* Compress PNG       : 2940   (0%)

testlandscape.png    : 8841600
GRAY SCALE :
* Compress BEST      : 2336194 (26%)
* Compress PNG       : 1637453 (18%)
RGB 565 :
* Compress BEST      : 2981992 (33%)
* Compress PNG       : 4013268 (45%)
RGB :
* Compress BEST      : 4328314 (48%)
* Compress PNG       : 4232793 (47%)
RGBA :
* Compress BEST      : 4843058 (54%)
* Compress PNG       : 4608015 (52%)

testicon.png         : 16384
GRAY SCALE :
* Compress BEST      : 2569 (15%)
* Compress PNG       : 1809 (11%)
RGB 565:
 Compress BEST       : 4661 (28%)
 Compress PNG        : 3893 (23%)
RGB:
* Compress BEST      : 5445 (33%)
* Compress PNG       : 3907 (23%)
RGBA:
* Compress BEST      : 6104 (37%)
* Compress PNG       : 4243 (25%)
 
testitem.png         : 16384
GRAY SCALE:
* Compress BEST      : 1306 (7%)
* Compress PNG       : 1119 (6%)
RGB_565:
* Compress BEST      : 3334 (20%)
* Compress PNG       : 2400 (14%)
RGB:
* Compress BEST      : 2365 (14%)
* Compress PNG       : 2372 (14%)
RGBA:
* Compress BEST      : 2796 (17%)
* Compress PNG       : 2608 (15%)
```

The less percentage, the best compression.
