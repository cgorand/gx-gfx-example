/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file PngImage.cpp
* @author GORAND Charles
*
*/

#include "PngImage.h"

#define GX_LOG_TAG "PngImage"
#include "gx/core/Log.h"

#include "libpng/png.h"

void png_read_from_mem(png_structp png_ptr, png_bytep data, png_size_t length)
{
	gx::Serializer *s = (gx::Serializer*)png_get_io_ptr(png_ptr);

	s->read(data, length);
}

void png_error_fn(png_structp png_ptr, png_const_charp error_msg)
{
	GX_ERROR("png_error: %s (%s)", error_msg, (char *)png_get_error_ptr(png_ptr));
	longjmp(png_jmpbuf(png_ptr), 1);
}

void png_warning_fn(png_structp png_ptr, png_const_charp warning_msg)
{
	GX_ERROR("png_warning: %s (%s)", warning_msg, (char *)png_get_error_ptr(png_ptr));
}


static void PngWriteCallback(png_structp  png_ptr, png_bytep data, png_size_t length)
{
	gx::Serializer *s = (gx::Serializer*)png_get_io_ptr(png_ptr);

	s->write(data,length);
}

PngImage::PngImage()
{
	m_image = new gx::Image();
}

PngImage::~PngImage()
{
	if (m_image)
	{
		delete m_image;
	}
}

gx::Image * PngImage::getImage()
{
	return m_image;
}

void PngImage::setImage(gx::Image* img)
{
	if (m_image)
	{
		delete m_image;
	}
	m_image = img;
}



void PngImage::serialize(gx::Serializer &s) const
{
	png_structp  p;
	png_infop    info_ptr;
	int          colorType;
	png_bytep   *row_pointers;
	int          bpp;

	p        = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	info_ptr = png_create_info_struct(p);

	switch (m_image->getFormat())
	{
		case gx::Image::GRAY_SCALE:
		{
			colorType = PNG_COLOR_TYPE_GRAY;
			bpp = 1;
			break;
		}

		case gx::Image::RGB_565:
		{
			GX_ERROR("RGB_565 not supported in PNG");
			return;
			break;
		}

		case gx::Image::RGB_888:
		{
			colorType = PNG_COLOR_TYPE_RGB;
			bpp = 3;
			break;
		}

		case gx::Image::RGBA_8888:
		{
			colorType = PNG_COLOR_TYPE_RGBA;
			bpp = 4;
			break;
		}

		default:
		{
			GX_ERROR("Unhandled case");
			return;
		}
	}

	setjmp(png_jmpbuf(p));
	png_set_IHDR(p, info_ptr, m_image->getWidth(), m_image->getHeight(), 8,
				 colorType,
				 PNG_INTERLACE_NONE,
				 PNG_COMPRESSION_TYPE_DEFAULT,
				 PNG_FILTER_TYPE_DEFAULT);

	row_pointers = new png_bytep[m_image->getHeight()];
	for (size_t y = 0; y < m_image->getHeight(); ++y)
	{
		row_pointers[y] = m_image->getSurface() + y * m_image->getWidth() * bpp;
	}

	png_set_rows(p, info_ptr, row_pointers);

	png_set_write_fn(p, &s, PngWriteCallback, NULL);

	png_write_png(p, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

	delete row_pointers;
	png_destroy_write_struct(&p, NULL);
}

void PngImage::deserialize(const gx::Serializer &s)
{
	png_structp    pngPtr;
	png_infop      infoPtr;
	int            bpp = 0;
	int            bit_depth;
	int            color_type;
	png_uint_32    w;
	png_uint_32    h;
	uint8_t       *surf;
	png_bytep     *row_pointers;

	const uint8_t      *imageData;
	gx::Image::eFormat  fmt;

	imageData = s.dataAtCurrentPosition();

	if (!png_check_sig((png_byte *)imageData, 8))
	{
		GX_ERROR("Invalid PNG signature");
		return;
	}

	/* create a png read struct */
	pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, png_error_fn, png_warning_fn);

	if (!pngPtr)
	{
		GX_ERROR("Invalid PNG version string");
		return;
	}

	/* create a png info struct */
	infoPtr = png_create_info_struct(pngPtr);

	if (!infoPtr)
	{
		png_destroy_read_struct(&pngPtr, NULL, NULL);
		GX_ERROR("Invalid PNG info");
		return;
	}

	if (setjmp(png_jmpbuf(pngPtr)))
	{
		png_destroy_read_struct(&pngPtr, &infoPtr, NULL);
		GX_ERROR("Invalid PNG data");
		return;
	}

	png_set_read_fn(pngPtr, (png_voidp) &s, png_read_from_mem);

	/* read png info */
	png_read_info(pngPtr, infoPtr);

	/* get some usefull information from header */
	bit_depth = png_get_bit_depth(pngPtr, infoPtr);
	color_type = png_get_color_type(pngPtr, infoPtr);

	/* convert index color images to RGB images */
	if (color_type == PNG_COLOR_TYPE_PALETTE)
	{
		png_set_palette_to_rgb(pngPtr);
	}

	if (bit_depth == 16)
	{
		png_set_strip_16(pngPtr);
	}
	else if (bit_depth < 8)
	{
		png_set_packing(pngPtr);
	}

	/* update info structure to apply transformations */
	png_read_update_info(pngPtr, infoPtr);

	/* retrieve updated information */
	png_get_IHDR(pngPtr, infoPtr, &w, &h, &bit_depth, &color_type, NULL, NULL, NULL);

	switch (color_type)
	{
		case PNG_COLOR_TYPE_GRAY_ALPHA:
		{
			GX_ERROR("Not supported : PNG_COLOR_TYPE_GRAY_ALPHA");
			return;
			break;
		}

		case PNG_COLOR_TYPE_GRAY:
		{
			fmt  = gx::Image::GRAY_SCALE;
			m_image->allocate(w, h, fmt);
			bpp = 1;
			break;
		}

		case PNG_COLOR_TYPE_RGB:
		{
			fmt  = gx::Image::RGB_888;
			m_image->allocate(w, h, fmt);
			bpp = 3;
			break;
		}

		case PNG_COLOR_TYPE_RGB_ALPHA:
		{
			fmt  = gx::Image::RGBA_8888;
			m_image->allocate(w, h, fmt);
			bpp = 4;
			break;
		}

		default:
		{
			GX_ERROR("Not supported");
			png_destroy_read_struct(&pngPtr, &infoPtr, NULL);
			return;
			break;
		}
	}

	surf = m_image->getSurface();

	/* setup a pointer array.  Each one points at the begening of a row. */
	row_pointers = new png_bytep[h];

	for (uint32_t i = 0; i < h; i++)
	{
		row_pointers[i] = (png_bytep)(surf + ((i) * w * bpp));
	}

	/* read pixel data using row pointers */
	png_read_image(pngPtr, row_pointers);


	png_read_end(pngPtr, NULL);
	png_destroy_read_struct(&pngPtr, &infoPtr, NULL);

	/* we don't need row pointers anymore */
	delete row_pointers;
}
