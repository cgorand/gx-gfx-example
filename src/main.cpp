/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file ImageConverterApplication.cpp
* @author GORAND Charles
*
*/

#include "gx/core/Serializer.h"
#include "gx/gfx/Image.h"
#include "gx/gfx/ImageConverterRGB.h"
#include "gx/gfx/ImageConverter565.h"
#include "gx/gfx/ImageConverterGrayScale.h"

#include "PngImage.h"

#define GX_LOG_TAG "ImageConverterApplication"
#include "gx/core/Log.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char tmpBuffer[255];

void toCppFile(const char *fileName, const char *varName, gx::Serializer &s)
{
	FILE *f;
	size_t size;
	const uint8_t *data;

	size = s.getSize();
	data = s.getData();

	f = fopen(fileName, "w");

	if (!f)
	{
		GX_LOG("Cannot write file %s.", fileName);
		exit(-1);
	}

	sprintf(tmpBuffer, "#include <stdint.h>\n\n");
	fwrite(tmpBuffer, strlen(tmpBuffer), 1, f);
	sprintf(tmpBuffer, "const uint8_t %s[]=\n", varName);
	fwrite(tmpBuffer, strlen(tmpBuffer), 1, f);
	sprintf(tmpBuffer, "{\n\t");
	fwrite(tmpBuffer, strlen(tmpBuffer), 1, f);

	for (size_t i = 0; i < size; i++)
	{
		sprintf(tmpBuffer, "0x%.2X,",data[i]);
		fwrite(tmpBuffer, strlen(tmpBuffer), 1, f);
		if (i % 16 == 0)
		{
			sprintf(tmpBuffer, "\n\t");
			fwrite(tmpBuffer, strlen(tmpBuffer), 1, f);
		}
	}
	sprintf(tmpBuffer, "\n};\n");
	fwrite(tmpBuffer, strlen(tmpBuffer), 1, f);

	fclose(f);
}


int main(int argc, char *argv[])
{
	const char         *execName;
	const char         *fileName;
	const char         *cppFileName;
	const char         *cppVarName;
	gx::Image::eFormat  destFormat;
	uint8_t            *imageData;
	size_t              imageSize;
	gx::Serializer      sIn;
	gx::Serializer      sOut;
	PngImage            png;
	char                debugFileName[255];
	gx::Image          *imgToConvert;
	size_t              initialSize;
	FILE               *f;

	gx::Image::eCompressAlgorithm algo;

	execName = argv[0];

	if (argc < 6)
	{
		GX_LOG("Usage :");
		GX_LOG("%s image.png image.cpp cpp_variable_name destination_format comp_algo", execName);
		GX_LOG("");
		GX_LOG("Where destination_format :");
		GX_LOG("gx::Image::eFormat::GRAY_SCALE = %i", gx::Image::eFormat::GRAY_SCALE);
		GX_LOG("gx::Image::eFormat::RGB_565    = %i", gx::Image::eFormat::RGB_565);
		GX_LOG("gx::Image::eFormat::RGB_888    = %i", gx::Image::eFormat::RGB_888);
		GX_LOG("gx::Image::eFormat::RGBA_8888  = %i", gx::Image::eFormat::RGBA_8888);
		GX_LOG("Where comp_algo :");
		GX_LOG("gx::Image::eCompressAlgorithm::NONE      = %i", gx::Image::eCompressAlgorithm::NONE);
		GX_LOG("gx::Image::eCompressAlgorithm::BEST      = %i", gx::Image::eCompressAlgorithm::BEST);
		GX_LOG("gx::Image::eCompressAlgorithm::LZW       = %i", gx::Image::eCompressAlgorithm::LZW);
		GX_LOG("gx::Image::eCompressAlgorithm::RLE_PLAIN = %i", gx::Image::eCompressAlgorithm::RLE_PLAIN);
		GX_LOG("gx::Image::eCompressAlgorithm::RLE_LZW   = %i", gx::Image::eCompressAlgorithm::RLE_LZW);
		exit(0);
	}

	GX_LOG("******************");
	fileName    = argv[1];
	cppVarName  = argv[3];
	cppFileName = argv[2];
	destFormat  = (gx::Image::eFormat) atoi(argv[4]);
	algo        = (gx::Image::eCompressAlgorithm) atoi(argv[5]);

	f = fopen(fileName, "r");

	if (!f)
	{
		GX_LOG("Cannot open file %s.", fileName);
		exit(-1);
	}

	sprintf(debugFileName, "%s_%i_%i_debug.png", fileName, destFormat, algo);

	fseek(f, 0, SEEK_END);
	imageSize = ftell(f);
	fseek(f, 0, SEEK_SET);  //same as rewind(f);

	imageData = new uint8_t[imageSize];
	fread(imageData, imageSize, 1, f);
	fclose(f);

	sIn.setStaticData(imageData, imageSize);

	png.deserialize(sIn);

	initialSize = png.getImage()->getWidth() * png.getImage()->getHeight() * png.getImage()->getBpp();

	GX_LOG("Initial size       : %i", initialSize);

	if (png.getImage()->getFormat() == gx::Image::RGB_888)
	{
		imgToConvert = gx::ImageConverterRGB::toRGBA(png.getImage());
		png.setImage(imgToConvert);
	}
	else
	{
		imgToConvert = png.getImage();
	}


	switch (destFormat)
	{
		case gx::Image::eFormat::GRAY_SCALE:
		{
			imgToConvert = gx::ImageConverterGrayScale::fromRGBA(imgToConvert);
			break;
		}

		case gx::Image::eFormat::RGB_565:
		{
			imgToConvert = gx::ImageConverter565::fromRGBA(imgToConvert);
			break;
		}

		case gx::Image::eFormat::RGB_888:
		{
			imgToConvert = gx::ImageConverterRGB::fromRGBA(imgToConvert);
			break;
		}

		case gx::Image::eFormat::RGBA_8888:
		{
			// Nothing to do
			break;
		}

		default:
		{
			GX_FATAL("Unhandled");
		}
	}

	sOut.clear();
	imgToConvert->setCompressAlgorithm(algo);
	imgToConvert->serialize(sOut);

	switch (algo)
	{
		case gx::Image::eCompressAlgorithm::NONE :
		{
			GX_LOG("Compress NONE      : %i (%i%%)", sOut.getSize(), sOut.getSize() * 100 / initialSize);
			break;
		}

		case gx::Image::eCompressAlgorithm::BEST :
		{
			GX_LOG("Compress BEST      : %i (%i%%)", sOut.getSize(), sOut.getSize() * 100 / initialSize);
			break;
		}

		case gx::Image::eCompressAlgorithm::LZW :
		{
			GX_LOG("Compress LZW       : %i (%i%%)", sOut.getSize(), sOut.getSize() * 100 / initialSize);
			break;
		}

		case gx::Image::eCompressAlgorithm::RLE_PLAIN :
		{
			GX_LOG("Compress RLE_PLAIN : %i (%i%%)", sOut.getSize(), sOut.getSize() * 100 / initialSize);
			break;
		}

		case gx::Image::eCompressAlgorithm::RLE_LZW:
		{
			GX_LOG("Compress RLE_LZW   : %i (%i%%)", sOut.getSize(), sOut.getSize() * 100 / initialSize);
			break;
		}

		default:
		{
			GX_FATAL("Unhandled algo");
		}
	}

	GX_LOG("cpp : %s", cppFileName);
	GX_LOG("var : %s", cppVarName);
	toCppFile(cppFileName, cppVarName, sOut);

	// Debug image
	sOut.rewind();
	imgToConvert->deserialize(sOut);

	// Convert back to 888 if needed
	if (imgToConvert->getFormat() == gx::Image::RGB_565)
	{
		imgToConvert = gx::ImageConverter565::toRGBA(imgToConvert);
	}

	if (imgToConvert != png.getImage())
	{
		png.setImage(imgToConvert);
	}

	sOut.clear();
	png.serialize(sOut);

	f = fopen(debugFileName, "w");
	fwrite(sOut.getData(), sOut.getSize(), 1, f);
	fclose(f);

	GX_LOG("Compress PNG       : %i (%i%%)", sOut.getSize(), sOut.getSize() * 100 / initialSize);
	return 0;
}

