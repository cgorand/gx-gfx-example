/**************************************************************************
* Copyright (C) 2018 GORAND Charles
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Developped by GORAND Charles : charles.gorand.dev@gmail.com
*
**************************************************************************/

/**
* @file PngImage.h
* @author GORAND Charles
*
*/

#ifndef GX_PNGIMAGE_H
#define GX_PNGIMAGE_H

#include "gx/gfx/Image.h"
#include "gx/core/Serializable.h"

class PngImage : public gx::Serializable
{
	public:
		PngImage();
		virtual ~PngImage();

		gx::Image *getImage();
		void setImage(gx::Image *img);

		virtual void serialize(gx::Serializer & s) const;
		virtual void deserialize(const gx::Serializer & s);

	private:
		gx::Image *m_image;
};

#endif // ndef GX_PNGIMAGE_H

