###########################################################################
# Copyright (C) 2017 GORAND Charles
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# Developped by Charles GORAND : charles.gorand.dev@gmail.com
# 
############################################################################

###########################################################
# GCC Specific configuration
###########################################################

ifeq ($(BUILD_TYPE),)
$(error "Define BUILD_TYPE : DEBUG or RELEASE")
endif

WARNING_FLAGS= \
	-Werror \
	-Wall \
	-Wclobbered \
	-Wempty-body \
	-Wignored-qualifiers \
	-Wmissing-field-initializers \
	-Wtype-limits \
	-Wuninitialized \
	-Wunused-but-set-parameter \
	-Wundef \
	-Wsign-compare

ifeq ($(BUILD_TYPE),DEBUG)
COMPILER_CFLAGS += \
	-ggdb
endif

ifeq ($(BUILD_TYPE),RELEASE)
COMPILER_CFLAGS += \
	-Os
endif

COMPILER_CFLAGS += \
	$(WARNING_FLAGS) \
	-c

GCC_INCLUDES+=
GCC_CFLAGS+= \
	$(COMPILER_CFLAGS) \
	-fdata-sections \
	-ffunction-sections

ifeq ($(OUTPUT_TYPE),binary)
GCC_LDFLAGS+= \
	-Wl,--gc-sections \
	-Xlinker -Map="$(OUTPUT_NAME).map" \
	-Xlinker -print-memory-usage
endif

GCC_CPP+=
GCC_C+=
GCC_ASM+=
GCC_MODULES+=

###########################################################
# Update project configuration
###########################################################
PROJECT_INCLUDES+=$(GCC_INCLUDES)
PROJECT_CFLAGS+=$(GCC_CFLAGS)
PROJECT_LDFLAGS+=$(GCC_LDFLAGS)
PROJECT_CPP_FILES+=$(GCC_CPP)
PROJECT_C_FILES+=$(GCC_C)
PROJECT_ASM_FILES+=$(GCC_ASM)
PROJECT_MODULES+=$(GCC_MODULES)
