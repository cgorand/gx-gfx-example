###########################################################################
# Copyright (C) 2010-2016 GORAND Charles
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Developped by GORAND Charles : charles.gorand.dev@gmail.com
#
############################################################################

###########################################################
# Misc configuration
###########################################################

# Use verbose ?
ifeq ($(VERBOSE),)
VERBOSE=@
endif

# Bin path
ifeq ($(BIN_PATH),)
BIN_PATH=bin
endif

# obj path
ifeq ($(OBJ_PATH),)
OBJ_PATH=obj
endif

# src path
ifeq ($(SRC_PATH),)
SRC_PATH=../src
endif
