###########################################################################
# Copyright (C) 2017 GORAND Charles
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# Developped by GORAND Charles : charles.gorand.dev@gmail.com
# 
############################################################################

###########################################################
# APPLICATION Sources
###########################################################

MODULES_INCLUDES+=
MODULES_CFLAGS+=
MODULES_LDFLAGS+=
MODULES_CPP+=
MODULES_C+=
MODULES_ASM+=
MODULES_MODULES+= \
	gx-gfx-standalone

###########################################################
# Update project configuration
###########################################################
PROJECT_INCLUDES+=$(MODULES_INCLUDES)
PROJECT_CFLAGS+=$(MODULES_CFLAGS)
PROJECT_LDFLAGS+=$(MODULES_LDFLAGS)
PROJECT_CPP_FILES+=$(MODULES_CPP)
PROJECT_C_FILES+=$(MODULES_C)
PROJECT_ASM_FILES+=$(MODULES_ASM)
PROJECT_MODULES+=$(MODULES_MODULES)
