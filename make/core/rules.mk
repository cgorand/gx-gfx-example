###########################################################################
# Copyright (C) 2010-2016 GORAND Charles
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Developped by GORAND Charles : charles.gorand.dev@gmail.com
#
############################################################################

###########################################################
# Rules file
###########################################################

###########################################################
# Basic Rules
###########################################################

#depends MakeFile :

DEPEND_MAKEFILE= \
	$(wildcard $(MAKEFILE_ROOT_PATH)pf/*.mk) \
	$(wildcard $(MAKEFILE_ROOT_PATH)pfcfg/*.mk) \
	$(wildcard $(MAKEFILE_ROOT_PATH)core/*.mk) \
	$(wildcard $(MAKEFILE_ROOT_PATH)cfg/*.mk) \
	$(MAKEFILE_ROOT_PATH)core/makefile \
	$(wildcard $(MAKEFILE_ROOT_PATH)*.mk) \
	$(wildcard $(MAKEFILE_PROJECT_PATH)*.mk)


# Build dependencies rule
define makeDepC
	$(ECHO) MAKEPEND $(notdir $@)
	$(MKDIR_CMD) obj
	$(MAKEDEPENDC) $(CFLAGS) $< -MF $@ -MT $(patsubst %.d,%.o,$@)
endef

define makeDepCpp
	$(ECHO) MAKEPEND $(notdir $@)
	$(MKDIR_CMD) obj
	$(MAKEDEPENDCPP) $(CFLAGS) $< -MF $@ -MT $(patsubst %.d,%.o,$@)
endef

# Build ASM file
define makeS
	$(ECHO) ASM $(notdir $<)
	$(CPP) $(CFLAGS) $(ASM_CFLAGS) -o $@ -c $<
endef
# Build C file
define makeC
	$(ECHO) CC $(notdir $<)
	$(CC) $(CFLAGS) -o $@ -c $<
endef
# Build CPP file
define makeCpp
	$(ECHO) CPP $(notdir $<)
	$(CPP) $(CFLAGS) -o $@ -c $<
endef

#Create CPP rules
define makeCppRules
$(OBJ_PATH)/$(notdir $(1:%.cpp=%.d)): $(1) $(DEPEND_MAKEFILE)
	$$(call makeDepCpp)
$(OBJ_PATH)/$(notdir $(1:%.cpp=%.o)): $(1) $(OBJ_PATH)/$(notdir $(1:%.cpp=%.d))
	$$(call makeCpp)
endef

#Create C rules
define makeCRules
$(OBJ_PATH)/$(notdir $(1:%.c=%.d)): $(1) $(DEPEND_MAKEFILE)
	$$(call makeDepC)
$(OBJ_PATH)/$(notdir $(1:%.c=%.o)): $(1) $(OBJ_PATH)/$(notdir $(1:%.c=%.d))
	$$(call makeC)
endef

#Create ASM rules
define makeASMRules
# $(OBJ_PATH)/$(notdir $(1:%.S=%.d)): $(1) $(DEPEND_MAKEFILE)
# 	$$(call makeDep)
$(OBJ_PATH)/$(notdir $(1:%.s=%.o)): $(1) $(DEPEND_MAKEFILE)
	$$(call makeS)
endef

define makeModuleGlobalRule
.PHONY: project_modules
project_modules: $(1)
$(1):
	+$(VERBOSE)$(MAKE) -f $(CURDIR)/makefile -C../../$$@/make/ MAKEFILE_ROOT_PATH=$(CURDIR)/ MAKEFILE_PROJECT_PATH=../../$$@/make/
endef

define makeModuleRules
PROJECT_INCLUDES+=$(call getModuleDefaultInclude,$(1))
PROJECT_LDFLAGS+=$(call getModuleStaticLib,$(1))
endef

#Create all rules
define makeRules
$(foreach file,$(D_FIND_CPP), $(eval $(call makeCppRules,$(SRC_PATH)/$(file))))
$(foreach file,$(D_FIND_C),  $(eval $(call makeCRules,$(SRC_PATH)/$(file))))
$(foreach file,$(D_FIND_ASM),$(eval $(call makeASMRules,$(SRC_PATH)/$(file))))
$(eval $(call makeModuleGlobalRule,$(PROJECT_MODULES)))
$(foreach file,$(PROJECT_MODULES), $(eval $(call makeModuleRules,$(file))))
$(foreach file,$(PROJECT_MODULES), $(eval $(call showDeps, $(file))))
endef

#Clean Modules rules
define cleanModuleRules
$(1).clean:
	$(ECHO) Clean Module $(1)
	+$(VERBOSE)$(MAKE) clean -f $(CURDIR)/makefile -C../../$(1)/make/ MAKEFILE_ROOT_PATH=$(CURDIR)/ MAKEFILE_PROJECT_PATH=../../$(1)/make/
endef

#Create clean rules
define cleanRules
$(foreach file,$(PROJECT_MODULES),$(eval $(call cleanModuleRules,$(file))))
endef

# Deps Modules rules
define depsModuleRules
$(1).deps:
	$(ECHO) $(1)
endef

#Create clean rules
define depsRules
$(foreach file,$(PROJECT_MODULES),$(eval $(call depsModuleRules,$(file))))
endef

###########################################################
# Sources management
###########################################################
# build info rules
define buildInfoRulesInternals
$(1).buildinfo:
	$(ECHO) Build Info $(1)
	+$(VERBOSE)$(MAKE) buildinfo -f $(CURDIR)/makefile -C../../$(1)/make/ MAKEFILE_ROOT_PATH=$(CURDIR)/ MAKEFILE_PROJECT_PATH=../../$(1)/make/
endef

#Create build info rules
define buildInfoRules
$(foreach file,$(PROJECT_MODULES),$(eval $(call buildInfoRulesInternals,$(file))))
endef

###########################################################
# Misc tools
###########################################################

define getModuleStaticLib
	../../$(1)/make/bin/$(STATICLIB_PREFIX)$(1)$(STATICLIB_SUFFIX)
endef

define getModuleDefaultInclude
	../../$(1)/src/
endef

define getModuleDefaultPath
	../../$(1)/
endef
