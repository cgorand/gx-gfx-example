###########################################################################
# Copyright (C) 2017 GORAND Charles
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# Developped by Charles GORAND : charles.gorand.dev@gmail.com
# 
############################################################################

###########################################################
# APPLICATION Sources
###########################################################

APPLICATION_INCLUDES+=

APPLICATION_CFLAGS+=
APPLICATION_LDFLAGS+= \
	-lpng
APPLICATION_CPP+= \
	main.cpp \
	PngImage.cpp

APPLICATION_C+=
APPLICATION_ASM+=
APPLICATION_MODULES+=

###########################################################
# Update project configuration
###########################################################
PROJECT_INCLUDES+=$(APPLICATION_INCLUDES)
PROJECT_CFLAGS+=$(APPLICATION_CFLAGS)
PROJECT_LDFLAGS+=$(APPLICATION_LDFLAGS)
PROJECT_CPP_FILES+=$(APPLICATION_CPP)
PROJECT_C_FILES+=$(APPLICATION_C)
PROJECT_ASM_FILES+=$(APPLICATION_ASM)
PROJECT_MODULES+=$(APPLICATION_MODULES)
