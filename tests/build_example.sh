#!/bin/bash

EXEC=../make/bin/gx-imageconverter

PATH_GX_GFX=../../gx-gfx-standalone/src

CXXFLAGS="-I../src -I${PATH_GX_GFX}"
LDFLAGS="-lpng"

# Compress image
$EXEC testitem.png testitem.cpp testitem 3 1

mkdir -p build/

g++ ${CXXFLAGS} -c main.cpp -o build/main.o
g++ ${CXXFLAGS} -c ../src/PngImage.cpp -o build/PngImage.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/core/Serializer.cpp -o build/Serializer.o

g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/linux/Log_linux.cpp                    -o build/Log_linux.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/lzw/lzw-lib.cpp                        -o build/lzw-lib.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/lzw/Lzw.cpp                         -o build/Lzw.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/gfx/Image.cpp                       -o build/Image.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/gfx/ImageConverter565.cpp           -o build/ImageConverter565.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/gfx/ImageConverterGrayScale.cpp     -o build/ImageConverterGrayScale.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/gfx/ImageConverterRGB.cpp           -o build/ImageConverterRGB.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/core/Serializer.cpp                 -o build/Serializer.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/core/Serializable.cpp               -o build/Serializable.o
g++ ${CXXFLAGS} -c ${PATH_GX_GFX}/gx/core/Log.cpp                        -o build/Log.o

g++  -o test_unpack build/*.o ${LDFLAGS}
