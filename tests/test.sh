#!/bin/bash

EXEC=../make/bin/gx-imageconverter


function testImage() {
	$EXEC test565.png test565.cpp test565 $1 $2
	$EXEC testlandscape.png testlandscape.cpp testlandscape $1 $2
	$EXEC testicon.png testicon.cpp testicon $1 $2
	$EXEC testitem.png testitem.cpp testitem $1 $2
}

# GRAY SCALE
testImage 1 1

# 565
testImage 2 1

# RGB
testImage 3 1

# RGBA
testImage 4 1

