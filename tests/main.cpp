/**************************************************************************
 * Copyright (C) 2020 GORAND Charles
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Developped by GORAND Charles : charles.gorand.dev@gmail.com
 *
 **************************************************************************/

/**
 * @file main.cpp
 * @author GORAND Charles
 *
 */

#include "testitem.cpp"

#include "PngImage.h"
#include "gx/gfx/Image.h"
#include "gx/core/Serializer.h"

#include <stdio.h>

int main(int argc, char *argv[])
{
	FILE               *f;

	gx::Serializer      s;
	gx::Image          *imgToExtract;

	// Use this to unpack image
	imgToExtract = new gx::Image();
	s.setStaticROData(testitem, sizeof(testitem));
	imgToExtract->deserialize(s);

	// Debug purpose
	PngImage            png;
	gx::Serializer      sPng;
	png.setImage(imgToExtract);
	png.serialize(sPng);

	f = fopen("unpacked.png", "w");
	fwrite(sPng.getData(), sPng.getSize(), 1, f);
	fclose(f);
}
