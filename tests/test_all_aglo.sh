#!/bin/bash

EXEC=../make/bin/gx-imageconverter


function testImage() {
	$EXEC test565.png test565.cpp test565 $1 $2
	$EXEC testlandscape.png testlandscape.cpp testlandscape $1 $2
	$EXEC testicon.png testicon.cpp testicon $1 $2
	$EXEC testitem.png testitem.cpp testitem $1 $2
}

# GRAY SCALE
testImage 1 0
testImage 1 2
testImage 1 3
testImage 1 4
testImage 1 1

# 565
testImage 2 0
testImage 2 2
testImage 2 3
testImage 2 4
testImage 2 1

# RGB
testImage 3 0
testImage 3 2
testImage 3 3
testImage 3 4
testImage 3 1

# RGBA
testImage 4 0
testImage 4 2
testImage 4 3
testImage 4 4
testImage 4 1

