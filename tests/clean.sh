#!/bin/bash

# Clean images
rm -f *png_*.png
# Clean generated sources
rm -f test*.cpp
# Clean build dir
rm -rf build/
